var url = document.location.href;
jQuery(document).ready(function($) {

    $('label[for=bp-login-widget-user-login]').text('Email:');

    //copy profile name to account name during registration
    if (url.indexOf("register/") >= 0) {
        $('label[for=signup_username],#signup_username,#field-visibility-settings-toggle-1').css('display','none');
        
        $('#field_1').blur(function() {
            $('#signup_username').val($('#field_1').val());
        });
    }

    //add disclaimer
    if (url.indexOf("location/") >=0) {
        $('#gmw-yl-address_autocomplete-title').append( $('<p class="steps"></p>').append( $('<span></span>').append("DISCLAIMER!<br> This is public. Don't put your full address unless you want to be visible to everyone.")) );
    }

    $('#agama-header-image').prepend( $('<span>Keeping adults off the street, one sport at a time</span>') );

});
