<?php

    function my_theme_enqueue_scripts() {      
        /** REGISTER HTML5 OtherScript.js **/
        wp_register_script( 'parent-script', get_stylesheet_directory_uri() . '/sportsii.js' );
        wp_enqueue_script( 'parent-script' );            
    }
    add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_scripts' );


    /* load css here */
    function my_theme_enqueue_styles() {
        wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    }
    add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );


    /* Prevent logged out users from accessing certain pages */
    function nonreg_visitor_redirect() {
        global $bp;
        if ( bp_is_activity_component() || bp_is_groups_component() || bp_is_group_forum() || is_page( 'members' ) || is_page( 'events' ) ) {
            if(!is_user_logged_in()) { //just a visitor and not logged in
                wp_redirect( get_option('siteurl') . '/home' );
            }
        }
    }
    add_filter('get_header','nonreg_visitor_redirect',1);


    function sportsii_add_event() {
        echo '<a href="' . bp_loggedin_user_domain() . '/events/my-events/?action=edit" class="button button-3d">Add Event</a>';
    }

    add_shortcode('ADD_EVENT', 'sportsii_add_event');

    /** Search Events Manager */
    add_filter('em_events_get_default_search','my_em_styles_get_default_search',1,2);
    function my_em_styles_get_default_search($searches, $array){
        if( !empty($array['style']) && is_numeric($array['style']) ){
            $searches['style'] = $array['style'];
        }
        return $searches;
    }

    add_filter('em_events_get','my_em_styles_events_get',1,2);
    function my_em_styles_events_get($events, $args){
        if( !empty($args['style']) && is_numeric($args['style']) ){
            foreach($events as $event_key => $EM_Event){
                if( !in_array($args['style'],$EM_Event->styles) ){
                    unset($events[$event_key]);
                }
            }
        }
        return $events;
    }
?>