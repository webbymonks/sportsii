<?php
/**
 * @package Boss Child Theme
 * The parent theme functions are located at /boss/buddyboss-inc/theme-functions.php
 * Add your own functions in this file.
 */

/**
 * Sets up theme defaults
 *
 * @since Boss Child Theme 1.0.0
 */
function boss_child_theme_setup()
{
  /**
   * Makes child theme available for translation.
   * Translations can be added into the /languages/ directory.
   * Read more at: http://www.buddyboss.com/tutorials/language-translations/
   */

  // Translate text from the PARENT theme.
  load_theme_textdomain( 'boss', get_stylesheet_directory() . '/languages' );

  // Translate text from the CHILD theme only.
  // Change 'boss' instances in all child theme files to 'boss_child_theme'.
  // load_theme_textdomain( 'boss_child_theme', get_stylesheet_directory() . '/languages' );

}
add_action( 'after_setup_theme', 'boss_child_theme_setup' );

/**
 * Enqueues scripts and styles for child theme front-end.
 *
 * @since Boss Child Theme  1.0.0
 */
function boss_child_theme_scripts_styles()
{
  /**
   * Scripts and Styles loaded by the parent theme can be unloaded if needed
   * using wp_deregister_script or wp_deregister_style.
   *
   * See the WordPress Codex for more information about those functions:
   * http://codex.wordpress.org/Function_Reference/wp_deregister_script
   * http://codex.wordpress.org/Function_Reference/wp_deregister_style
   **/

  /*
   * Styles
   */
  wp_enqueue_style( 'boss-child-custom', get_stylesheet_directory_uri().'/css/custom.css' );

  
}
add_action( 'wp_enqueue_scripts', 'boss_child_theme_scripts_styles', 9999 );


/****************************** CUSTOM FUNCTIONS ******************************/

// Add your own custom functions here

function my_theme_enqueue_scripts() {      
    wp_register_script( 'parent-script', get_stylesheet_directory_uri() . '/js/custom.js' );
    wp_enqueue_script( 'parent-script' );           
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_scripts', 9998 );

/* Prevent logged out users from accessing certain pages */
function nonreg_visitor_redirect() {
    global $bp;
    if ( bp_is_activity_component() || bp_is_groups_component() || bp_is_group_forum() || is_page( 'members' ) || is_page( 'events' ) ) {
        if(!is_user_logged_in()) { //just a visitor and not logged in
            wp_redirect( get_option('siteurl') . '/home' );
        }
    }
}
add_filter('get_header','nonreg_visitor_redirect',1);

function redirect_to_activity() {
    return home_url();
    
}
add_filter('login_redirect', 'redirect_to_activity', 10, 3);

?>