<?php /* Template Name: Custom */ ?>
<?php  

echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('stylesheet_directory').'/css/home.css?'. time() .'">'."\n";

add_shortcode( 'site_tagline' , 'showTagline' );

function  showTagline() {
    echo get_bloginfo('description');
}

$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
$thumb_url = $thumb_url_array[0];

 ?>
<link rel="stylesheet" id="font-style-Montserrat-css" href="http://fonts.googleapis.com/css?family=Montserrat&amp;ver=4.8" type="text/css" media="all">

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-7286502704777438",
    enable_page_level_ads: true
  });
</script>

<div id="primary" class="content-area">
    <div class="mainbg" style="background: url(<?php echo $thumb_url; ?>) no-repeat center center fixed; "></div>
    <main id="main" class="site-main" role="main">

    <?php
    // Start the loop.
    while ( have_posts() ) : the_post();

        // Include the page content template.
        get_template_part( 'content', 'page' );

        // If comments are open or we have at least one comment, load up the comment template.
        if ( comments_open() || get_comments_number() ) :
            comments_template();
        endif;

    // End the loop.
    endwhile;
    ?>

    </main><!-- .site-main -->
    <div class="homeHeader">
        <img src="http://sportsii.com/sportsii_new/wp-content/uploads/2017/06/logo_280x80-1.png" width="280" height="80"/>
        <a href="../">Sportii Home</a>
    </div>
</div><!-- .content-area -->