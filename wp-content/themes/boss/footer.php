<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Boss
 * @since Boss 1.0.0
 */
?>
</div><!-- #main .wrapper -->

</div><!-- #page -->

</div> <!-- #inner-wrap -->

</div><!-- #main-wrap (Wrap For Mobile) -->

<footer id="colophon" role="contentinfo">

	<?php get_template_part( 'template-parts/footer-widgets' ); ?>

	<div class="footer-inner-bottom">

		<div class="footer-inner">
			<?php get_template_part( 'template-parts/footer-copyright' ); ?>
			<?php get_template_part( 'template-parts/footer-links' ); ?>
		</div><!-- .footer-inner -->

	</div><!-- .footer-inner-bottom -->

	<?php do_action( 'bp_footer' ) ?>

</footer><!-- #colophon -->
</div><!-- #right-panel-inner -->
</div><!-- #right-panel -->

</div><!-- #panels -->

<?php

$url = json_decode(file_get_contents("http://api.ipinfodb.com/v3/ip-city/?key=2b3d7d0ad1a285279139487ce77f3f58d980eea9546b5ccc5d08f5ee62ce7471&ip=".$_SERVER['REMOTE_ADDR']."&format=json"));

?>
<?php wp_footer(); ?>
<script>
$(document).ready(function(){ 
  <?php if(ICL_LANGUAGE_CODE=='fr'){ ?>
    $('input[placeholder="Enter Address..."]').attr('placeholder', "Entrez l'adresse")
   <?php  } ?>
});
</script>
<script type="text/javascript">
$(document).ready(function() {
	 var ipadd = '<?php echo $url->zipCode; ?>';
	 var country = '<?php echo $url->countryName; ?>';
    $("#field_394").val(ipadd);
	$("#field_366").val(country);	
});
</script>
</body>
</html>