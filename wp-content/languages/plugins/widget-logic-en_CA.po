# Translation of Stable (latest release) in English (Canada)
# This file is distributed under the same license as the Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2015-11-06 06:55:50+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/1.0-alpha-1100\n"
"Project-Id-Version: Stable (latest release)\n"

#: widget_logic.php:178
msgid "Add 'widget_content' filter"
msgstr "Add 'widget_content' filter"

#: widget_logic.php:176
msgid "Adds a new WP filter you can use in your own code. Not needed for main Widget Logic functionality."
msgstr "Adds a new WP filter you can use in your own code. Not needed for main Widget Logic functionality."

#: widget_logic.php:19
msgid "after theme loads"
msgstr "after theme loads"

#. Author of the plugin/theme
msgid "Alan Trewartha"
msgstr "Alan Trewartha"

#. Description of the plugin/theme
msgid "Control widgets with WP's conditional tags is_home etc"
msgstr "Control widgets with WP's conditional tags (is_home, etc.)"

#: widget_logic.php:186
msgid "Delays widget logic code being evaluated til various points in the WP loading process"
msgstr "Delays widget logic code being evaluated til various points in the WP loading process."

#: widget_logic.php:21
msgid "during page header"
msgstr "during page header"

#: widget_logic.php:203
msgid "Export options"
msgstr "Export options"

#. Author URI of the plugin/theme
msgid "http://freakytrigger.co.uk/author/alan/"
msgstr "http://freakytrigger.co.uk/author/alan/"

#. Plugin URI of the plugin/theme
msgid "http://wordpress.org/extend/plugins/widget-logic/"
msgstr "http://wordpress.org/extend/plugins/widget-logic/"

#: widget_logic.php:204
msgid "Import options"
msgstr "Import options"

#: widget_logic.php:99
msgid "Invalid options file"
msgstr "Invalid options file"

#: widget_logic.php:204
msgid "Load all WL options from a plain text config file"
msgstr "Load all WL options from a plain text config file."

#: widget_logic.php:186
msgid "Load logic"
msgstr "Load logic"

#: widget_logic.php:104
msgid "No options file provided"
msgstr "No options file provided."

#: widget_logic.php:181
msgid "Resets a theme's custom queries before your Widget Logic is checked"
msgstr "Resets a theme's custom queries before your Widget Logic is checked."

#: widget_logic.php:203
msgid "Save all WL options to a plain text config file"
msgstr "Save all WL options to a plain text config file."

#: widget_logic.php:199
msgid "Save WL options"
msgstr "Save WL options"

#: widget_logic.php:205
msgid "Select file for importing"
msgstr "Select file for importing"

#: widget_logic.php:96
msgid "Success! Options file imported"
msgstr "Success! Options file imported."

#: widget_logic.php:183
msgid "Use 'wp_reset_query' fix"
msgstr "Use 'wp_reset_query' fix"

#: widget_logic.php:20
msgid "when all PHP loaded"
msgstr "when all PHP loaded"

#: widget_logic.php:18
msgid "when plugin starts (default)"
msgstr "when plugin starts (default)"

#. Plugin Name of the plugin/theme
msgid "Widget Logic"
msgstr "Widget Logic"

#: widget_logic.php:173
msgid "Widget Logic options"
msgstr "Widget Logic options"

#: widget_logic.php:241
msgid "Widget logic:"
msgstr "Widget logic:"
