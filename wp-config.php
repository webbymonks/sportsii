<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sportsii_new');

/** MySQL database username */
define('DB_USER', 'sportsii');

/** MySQL database password */
define('DB_PASSWORD', 'P455w0rd!@#');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** BuddyPress */
define('BP_DEFAULT_COMPONENT', 'profile' );

define ('EM_WPML_FORCE_RECURRENCES',true);


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Wr>u:N~^=;Jg/R0WY>r9Eu hIiCuo]}9mP)2bIS86#-I8Q;wOV14xOb5T=H,YsO+');
define('SECURE_AUTH_KEY',  '_X7{(>[qqa2) jA4j>0J#0Hd/od*N-)B-M|}8XIO-a%,Xaq/?hkO. u&nDipRcxg');
define('LOGGED_IN_KEY',    'CDNnb]V|?=3*V{VE(H*`?qoQ:rn0Hu#uOJd%CyRx#zj=`tccH#J7z`/B:EC|QQ$x');
define('NONCE_KEY',        'Pl!v8^aS4}ZCe<!%{yUp(!5R-r*_=n2z#FMu<go)[~}_5J[(U8dd0;}{ oz.Hb]l');
define('AUTH_SALT',        'TC3]B~c/w[(+LZ_Pl[#/4wEo.NPn[V5yD~%q7Ss8=7%5jlx:At&P~ALnY?DnmnYy');
define('SECURE_AUTH_SALT', ')%RKc9,]zW?CHBbc[4ZN:vT$3vX2$K;%>MgMQoB0DnwFEo|h[r@s>4>D3J9Npi]Z');
define('LOGGED_IN_SALT',   'Kl*)@7.zhP[ ycv5LmcX$_]HsHbv50xIl|s=D6=3u]2/dc+mHj5,=nj^J4HsXq}4');
define('NONCE_SALT',       'g5mw;abKX`P%]19c7B%{`/cLpt~d|VN|<=;i7(/=nSr!}yE^fG*X_il2=(Tg!-&D');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
