<?php 
// Multiple recipients
$to = 'john.deo4566@gmail.com'; // note the comma

// Subject
$subject = 'Project server';

// Message
$message = '
<html>
<head>
  <title>Project</title>
</head>
<body>
  
  <table>
    
    <tr>
      <td>Test</td><td>10th</td><td>August</td><td>1970</td>
    </tr>
    <tr>
      <td>Demo</td><td>17th</td><td>August</td><td>1973</td>
    </tr>
  </table>
</body>
</html>
';

// To send HTML mail, the Content-type header must be set
$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-type: text/html; charset=iso-8859-1';

// Additional headers
$headers[] = 'From: Test <john@example.com>';

// Mail it
mail($to, $subject, $message, implode("\r\n", $headers));
?>